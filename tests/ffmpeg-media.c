#include <gmu/gmu.h>
#include <gtk/gtk.h>

static void
test_ffmpeg_media_load (void)
{
  GError *error = NULL;
  g_autoptr (GInputStream) movie_stream = NULL;
  g_autoptr (GmuFFmpegMedia) ffmeg_media = NULL;

  movie_stream = g_resources_open_stream ("/org/feliwir/gmu/Tests/ffmpeg/sintel-1024-surround.mp4", G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  g_assert_no_error (error);

  ffmeg_media = gmu_ffmpeg_media_new_for_input_stream (movie_stream);
  g_assert_nonnull (ffmeg_media);
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ffmpeg-media/load", test_ffmpeg_media_load);

  return g_test_run ();
}
