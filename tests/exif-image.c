#include <gmu/gmu.h>
#include <gtk/gtk.h>

static void
test_exif_image_gps (void)
{
  GError *error = NULL;
  g_autoptr (GBytes) exif_img_data = NULL;
  g_autoptr (GmuExifImage) exif_img = NULL;

  exif_img_data = g_resources_lookup_data ("/org/feliwir/gmu/Tests/gps/DSCN0025.jpg", G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  g_assert_no_error (error);

  exif_img = gmu_exif_image_new_from_bytes (exif_img_data);
  g_assert_nonnull (exif_img);

  gdouble latitude, longitude;
  g_assert_true (gmu_exif_image_try_get_latitude (exif_img, &latitude));
  g_assert_true (gmu_exif_image_try_get_longitude (exif_img, &longitude));

  g_assert_cmpint (gmu_exif_image_get_orientation (exif_img), ==, GMU_EXIF_ORIENTATION_NORMAL);
}

static void
test_exif_image_orientation_none (void)
{
  GError *error = NULL;
  g_autoptr (GBytes) exif_img_data = NULL;
  g_autoptr (GmuExifImage) exif_img = NULL;

  exif_img_data = g_resources_lookup_data ("/org/feliwir/gmu/Tests/orientation/landscape_1.jpg", G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  g_assert_no_error (error);

  exif_img = gmu_exif_image_new_from_bytes (exif_img_data);
  g_assert_nonnull (exif_img);

  // This img should have no GPS data
  gdouble latitude, longitude;
  g_assert_false (gmu_exif_image_try_get_latitude (exif_img, &latitude));
  g_assert_false (gmu_exif_image_try_get_longitude (exif_img, &longitude));

  g_assert_cmpint (gmu_exif_image_get_orientation (exif_img), ==, GMU_EXIF_ORIENTATION_NORMAL);
}

static void
test_exif_image_orientation_hflip (void)
{
  GError *error = NULL;
  g_autoptr (GBytes) exif_img_data = NULL;
  g_autoptr (GmuExifImage) exif_img = NULL;

  exif_img_data = g_resources_lookup_data ("/org/feliwir/gmu/Tests/orientation/landscape_2.jpg", G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  g_assert_no_error (error);

  exif_img = gmu_exif_image_new_from_bytes (exif_img_data);
  g_assert_nonnull (exif_img);

  // This img should have no GPS data
  gdouble latitude, longitude;
  g_assert_false (gmu_exif_image_try_get_latitude (exif_img, &latitude));
  g_assert_false (gmu_exif_image_try_get_longitude (exif_img, &longitude));

  g_assert_cmpint (gmu_exif_image_get_orientation (exif_img), ==, GMU_EXIF_ORIENTATION_HFLIP);
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/exif-image/gps", test_exif_image_gps);

  g_test_add_func ("/exif-image/orientation/none", test_exif_image_orientation_none);
  g_test_add_func ("/exif-image/orientation/hflip", test_exif_image_orientation_hflip);

  return g_test_run ();
}
