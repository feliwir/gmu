GMU — The GTK media utilities library
=====================

General information
-------------------

The following convenience widgets / classes are available:
  - [a paintable that takes care of EXIF orientation](./gmu/gmu-exif-image.h)

Documentation:
  - https://feliwir.pages.gitlab.gnome.org/gmu/

Building and installing
-----------------------

In order to build GMU you will need:

  - a C11 compatible compiler
  - [Meson](http://mesonbuild.com)
  - [Ninja](https://ninja-build.org)