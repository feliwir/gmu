/*
 * Copyright (c) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <gdk/gdk.h>

G_BEGIN_DECLS

#define GMU_TYPE_FFMPEG_MEDIA (gmu_ffmpeg_media_get_type ())
G_DECLARE_FINAL_TYPE (GmuFFmpegMedia, gmu_ffmpeg_media, GMU, FFMPEG_MEDIA, GObject)

GmuFFmpegMedia *gmu_ffmpeg_media_new_for_file (GFile *file);
GmuFFmpegMedia *gmu_ffmpeg_media_new_for_input_stream (GInputStream *stream);

void gmu_ffmpeg_media_clear (GmuFFmpegMedia *self);
gboolean gmu_ffmpeg_media_seek (GmuFFmpegMedia *self, int64_t timestamp);
gboolean gmu_ffmpeg_media_process (GmuFFmpegMedia *self);

void gmu_ffmpeg_media_set_file (GmuFFmpegMedia *self,
                                GFile *file);
GFile *gmu_ffmpeg_media_get_file (GmuFFmpegMedia *self);

void gmu_ffmpeg_media_set_input_stream (GmuFFmpegMedia *self,
                                        GInputStream *stream);
GInputStream *gmu_ffmpeg_media_get_input_stream (GmuFFmpegMedia *self);

gint64 gmu_ffmpeg_media_get_timestamp (GmuFFmpegMedia *self);
gint64 gmu_ffmpeg_media_get_duration (GmuFFmpegMedia *self);

gboolean gmu_ffmpeg_media_is_seeking (GmuFFmpegMedia *self);

G_END_DECLS
