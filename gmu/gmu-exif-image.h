/*
 * Copyright (c) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <gdk/gdk.h>

G_BEGIN_DECLS

/**
 * GmuExifOrientation:
 * @GMU_EXIF_ORIENTATION_UNSPECIFIED: The orientation of the image is unknown
 * @GMU_EXIF_ORIENTATION_NORMAL: The orientation of the image is without any rotation.
 * @GMU_EXIF_ORIENTATION_HFLIP: The image is flipped on its horizontal axis
 * @GMU_EXIF_ORIENTATION_ROT_180: The image is rotated by 180 degrees
 * @GMU_EXIF_ORIENTATION_VFLIP: The Image is flipped on its vertical axis
 * @GMU_EXIF_ORIENTATION_ROT_90_HFLIP: The image is rotated by 90 degrees clockwise and flipped on its horizontal axis
 * @GMU_EXIF_ORIENTATION_ROT_90: The image is rotated by 90 degrees clockwise
 * @GMU_EXIF_ORIENTATION_ROT_90_VFLIP: The image is rotated by 90 degrees clockwise and flipped on its vertical axis
 * @GMU_EXIF_ORIENTATION_ROT_270: The image is rotated 270 degrees clockwise
 *
 * The orientation of an image is defined as the location of it's x,y origin.  More than rotation,
 * orientation allows for every variation of rotation, flips, and mirroring to be described in
 * 3 bits of data.
 *
 * A handy primer to orientation can be found at
 * <ulink url="http://jpegclub.org/exif_orientation.html"></ulink>
 */
typedef enum
{
  /*< private >*/
  GMU_EXIF_ORIENTATION_MIN = 0,
  /*< public >*/
  GMU_EXIF_ORIENTATION_UNSPECIFIED = 0,
  GMU_EXIF_ORIENTATION_NORMAL = 1,
  GMU_EXIF_ORIENTATION_HFLIP = 2,
  GMU_EXIF_ORIENTATION_ROT_180 = 3,
  GMU_EXIF_ORIENTATION_VFLIP = 4,
  GMU_EXIF_ORIENTATION_ROT_90_HFLIP = 5,
  GMU_EXIF_ORIENTATION_ROT_90 = 6,
  GMU_EXIF_ORIENTATION_ROT_90_VFLIP = 7,
  GMU_EXIF_ORIENTATION_ROT_270 = 8,
  /*< private >*/
  GMU_EXIF_ORIENTATION_MAX = 8
} GmuExifOrientation;

#define GMU_TYPE_EXIF_IMAGE (gmu_exif_image_get_type ())
G_DECLARE_FINAL_TYPE (GmuExifImage, gmu_exif_image, GMU, EXIF_IMAGE, GObject)

GmuExifImage *gmu_exif_image_new_from_file (GFile *file);
void gmu_exif_image_set_from_file (GmuExifImage *self, GFile *file);

GmuExifImage *gmu_exif_image_new_from_bytes (GBytes *bytes);
void gmu_exif_image_set_from_bytes (GmuExifImage *self, GBytes *bytes);

GmuExifOrientation gmu_exif_image_get_orientation (GmuExifImage *self);

gboolean gmu_exif_image_try_get_latitude (GmuExifImage *self, gdouble *latitude);
gboolean gmu_exif_image_try_get_longitude (GmuExifImage *self, gdouble *longitude);

G_END_DECLS
