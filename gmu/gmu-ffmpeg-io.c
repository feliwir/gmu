/*
 * Copyright (c) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "gmu-enum-types.h"
#include "gmu-ffmpeg-io-private.h"

static int
gmu_ff_io_read_cb (void *data,
                   uint8_t *buf,
                   int buf_size)
{
  GInputStream *input_stream = data;
  GError *error = NULL;
  gssize n_read;

  n_read = g_input_stream_read (input_stream,
                                buf,
                                buf_size,
                                NULL,
                                &error);
  if (n_read < 0)
    {
      // TODO: log an error
    }
  else if (n_read == 0)
    {
      n_read = AVERROR_EOF;
    }

  return n_read;
}

static int
gmu_ff_io_write_cb (void *data,
                    uint8_t *buf,
                    int buf_size)
{
  GOutputStream *output_stream = data;
  GError *error = NULL;
  gssize n_read;

  n_read = g_output_stream_write (output_stream,
                                  buf,
                                  buf_size,
                                  NULL,
                                  &error);
  if (n_read < 0)
    {
      // TODO: log an error
    }
  else if (n_read == 0)
    {
      n_read = AVERROR_EOF;
    }

  return n_read;
}

static int64_t
gmu_ff_io_seek_cb (void *data,
                   int64_t offset,
                   int whence)
{
  GInputStream *input_stream = data;
  GSeekType seek_type;
  gboolean result;

  switch (whence)
    {
    case SEEK_SET:
      seek_type = G_SEEK_SET;
      break;

    case SEEK_CUR:
      seek_type = G_SEEK_CUR;
      break;

    case SEEK_END:
      seek_type = G_SEEK_END;
      break;

    case AVSEEK_SIZE:
      /* FIXME: Handle size querying */
      return -1;

    default:
      g_assert_not_reached ();
      return -1;
    }

  result = g_seekable_seek (G_SEEKABLE (input_stream),
                            offset,
                            seek_type,
                            NULL,
                            NULL);
  if (!result)
    return -1;

  return g_seekable_tell (G_SEEKABLE (input_stream));
}

AVIOContext *
gmu_ff_create_input_io_context (GInputStream *input_stream)
{
  AVIOContext *result;
  int buffer_size = 4096; /* it's what everybody else uses... */
  unsigned char *buffer;

  buffer = av_malloc (buffer_size);
  if (buffer == NULL)
    return NULL;

  result = avio_alloc_context (buffer,
                               buffer_size,
                               AVIO_FLAG_READ,
                               input_stream,
                               gmu_ff_io_read_cb,
                               NULL,
                               G_IS_SEEKABLE (input_stream)
                                   ? gmu_ff_io_seek_cb
                                   : NULL);

  result->buf_ptr = result->buf_end;
  result->write_flag = 0;

  return result;
}

AVIOContext *
gmu_ff_create_output_io_context (GOutputStream *output_stream)
{
  AVIOContext *result;
  int buffer_size = 4096; /* it's what everybody else uses... */
  unsigned char *buffer;

  buffer = av_malloc (buffer_size);
  if (buffer == NULL)
    return NULL;

  result = avio_alloc_context (buffer,
                               buffer_size,
                               AVIO_FLAG_WRITE,
                               output_stream,
                               NULL,
                               gmu_ff_io_write_cb,
                               NULL);

  result->buf_ptr = result->buf_end;
  result->write_flag = 1;

  return result;
}