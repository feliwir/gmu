/*
 * Copyright (c) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "gmu-ffmpeg-stream.h"

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>

G_BEGIN_DECLS

GmuFFmpegStream *gmu_ffmpeg_stream_new_from_avdata (AVStream *stream);

gboolean gmu_ffmpeg_stream_process_packet (GmuFFmpegStream *self, AVPacket *packet);

AVCodecContext *gmu_ffmpeg_stream_get_codec (GmuFFmpegStream *self);

void gmu_ffmpeg_stream_set_stream (GmuFFmpegStream *self, AVStream *stream);
AVStream *gmu_ffmpeg_stream_get_stream (GmuFFmpegStream *self);

G_END_DECLS
