/*
 * Copyright (c) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "gmu-ffmpeg-stream.h"
#include "gmu-ffmpeg-stream-private.h"

#include <glib/gi18n.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>

/**
 * GmuFFmpegStream:
 *
 * An audio, video or subtitle stream, which was retrieved by demuxing or to be added by a muxer
 *
 * Since: 0.1
 */
struct _GmuFFmpegStream
{
  GObject parent_instance;

  GError *error;

  AVStream *stream;
  AVCodecContext *codec_ctx;
  AVFrame *frame;
};

enum
{
  PROP_0,
  PROP_ERROR,
  PROP_STREAM,

  N_PROPS,
};

static GParamSpec *properties[N_PROPS] = {
  NULL,
};

G_DEFINE_TYPE (GmuFFmpegStream,
               gmu_ffmpeg_stream,
               G_TYPE_OBJECT)

static void
gmu_ffmpeg_stream_gerror (GmuFFmpegStream *self,
                          GError *error)
{
  g_return_if_fail (GMU_IS_FFMPEG_STREAM (self));
  g_return_if_fail (error != NULL);

  if (self->error)
    {
      g_error_free (error);
      return;
    }

  g_object_freeze_notify (G_OBJECT (self));

  self->error = error;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ERROR]);

  g_object_thaw_notify (G_OBJECT (self));
}

static void
gmu_ffmpeg_stream_error (GmuFFmpegStream *self,
                         GQuark domain,
                         int code,
                         const char *format,
                         ...)
{
  GError *error;
  va_list args;

  g_return_if_fail (GMU_IS_FFMPEG_STREAM (self));
  g_return_if_fail (domain != 0);
  g_return_if_fail (format != NULL);

  va_start (args, format);
  error = g_error_new_valist (domain, code, format, args);
  va_end (args);

  gmu_ffmpeg_stream_gerror (self, error);
}

static void
gmu_ffmpeg_stream_set_ffmpeg_error (GmuFFmpegStream *self,
                                    int av_errnum)
{
  char s[AV_ERROR_MAX_STRING_SIZE];

  if (self->error)
    return;

  if (av_strerror (av_errnum, s, sizeof (s) != 0))
    g_snprintf (s, sizeof (s), _ ("Unspecified stream error"));

  gmu_ffmpeg_stream_error (self,
                           G_IO_ERROR,
                           G_IO_ERROR_FAILED,
                           "%s",
                           s);
}

static void
gmu_ffmpeg_stream_set_property (GObject *object,
                                guint prop_id,
                                const GValue *value,
                                GParamSpec *pspec)

{
  GmuFFmpegStream *self = GMU_FFMPEG_STREAM (object);

  switch (prop_id)
    {
    case PROP_STREAM:
      gmu_ffmpeg_stream_set_stream (self, g_value_get_pointer (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gmu_ffmpeg_stream_get_property (GObject *object,
                                guint prop_id,
                                GValue *value,
                                GParamSpec *pspec)
{
  GmuFFmpegStream *self = GMU_FFMPEG_STREAM (object);

  switch (prop_id)
    {
    case PROP_ERROR:
      g_value_set_boxed (value, self->error);
      break;

    case PROP_STREAM:
      g_value_set_pointer (value, self->stream);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gmu_ffmpeg_stream_dispose (GObject *object)
{
  GmuFFmpegStream *self = GMU_FFMPEG_STREAM (object);

  g_clear_pointer (&self->codec_ctx, avcodec_close);
  av_frame_free (&self->frame);
  self->stream = NULL;

  G_OBJECT_CLASS (gmu_ffmpeg_stream_parent_class)->dispose (object);
}

static void
gmu_ffmpeg_stream_class_init (GmuFFmpegStreamClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->set_property = gmu_ffmpeg_stream_set_property;
  gobject_class->get_property = gmu_ffmpeg_stream_get_property;
  gobject_class->dispose = gmu_ffmpeg_stream_dispose;

  /**
   * GmuFFmpegStream:error:
   *
   * %NULL for a properly working stream or the `GError`
   * that the stream is in.
   */
  properties[PROP_ERROR] =
      g_param_spec_boxed ("error", NULL, NULL,
                          G_TYPE_ERROR,
                          G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * GmuFFmpegStream:stream
   *
   * The underlying AVStream handle.
   */
  properties[PROP_STREAM] =
      g_param_spec_pointer ("stream", NULL, NULL,
                            G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (gobject_class, N_PROPS, properties);
}

static void
gmu_ffmpeg_stream_open (GmuFFmpegStream *self)
{
  int errnum;
  const AVCodec *codec;

  g_assert (self->stream != NULL);

  codec = avcodec_find_decoder (self->stream->codecpar->codec_id);
  if (codec == NULL)
    {
      gmu_ffmpeg_stream_error (self,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_SUPPORTED,
                               _ ("Cannot find decoder: %s"),
                               avcodec_get_name (self->stream->codecpar->codec_id));
      return;
    }
  self->codec_ctx = avcodec_alloc_context3 (codec);
  if (self->codec_ctx == NULL)
    {
      gmu_ffmpeg_stream_error (self,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_SUPPORTED,
                               _ ("Failed to allocate a codec context"));
      return;
    }
  errnum = avcodec_parameters_to_context (self->codec_ctx, self->stream->codecpar);
  if (errnum < 0)
    {
      gmu_ffmpeg_stream_set_ffmpeg_error (self, errnum);
      return;
    }
  errnum = avcodec_open2 (self->codec_ctx, codec, &self->stream->metadata);
  if (errnum < 0)
    {
      gmu_ffmpeg_stream_set_ffmpeg_error (self, errnum);
      return;
    }
}

static void
gmu_ffmpeg_stream_init (GmuFFmpegStream *self)
{
  self->frame = av_frame_alloc ();
}

AVCodecContext *
gmu_ffmpeg_stream_get_codec (GmuFFmpegStream *self)
{
  g_return_val_if_fail (GMU_IS_FFMPEG_STREAM (self), NULL);

  return self->codec_ctx;
}

void
gmu_ffmpeg_stream_set_stream (GmuFFmpegStream *self, AVStream *stream)
{
  g_return_if_fail (GMU_IS_FFMPEG_STREAM (self));
  g_return_if_fail (stream != NULL);

  g_object_freeze_notify (G_OBJECT (self));

  self->stream = stream;
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_STREAM]);
  gmu_ffmpeg_stream_open (self);

  g_object_thaw_notify (G_OBJECT (self));
}

AVStream *
gmu_ffmpeg_stream_get_stream (GmuFFmpegStream *self)
{
  g_return_val_if_fail (GMU_IS_FFMPEG_STREAM (self), NULL);

  return self->stream;
}

GmuFFmpegStream *
gmu_ffmpeg_stream_new_from_avdata (AVStream *avstream)
{
  g_return_val_if_fail (avstream != NULL, NULL);

  GmuFFmpegStream *stream;

  stream = g_object_new (GMU_TYPE_FFMPEG_STREAM,
                         "stream", avstream,
                         NULL);

  return stream;
}

gboolean
gmu_ffmpeg_stream_process_packet (GmuFFmpegStream *self, AVPacket *packet)
{
  int errnum;

  errnum = avcodec_send_packet (self->codec_ctx, packet);
  if (errnum < 0)
    {
      gmu_ffmpeg_stream_set_ffmpeg_error (self, errnum);
      return FALSE;
    }

  errnum = avcodec_receive_frame (self->codec_ctx, self->frame);
  if (errnum == AVERROR (EAGAIN))
    {
      // Just wait for the next packet
      return FALSE;
    }

  if (errnum < 0)
    {
      gmu_ffmpeg_stream_set_ffmpeg_error (self, errnum);
      return FALSE;
    }
  else
    {
      av_packet_unref (packet);
    }

  return TRUE;
}