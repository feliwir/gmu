/*
 * Copyright (c) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "gmu-exif-image.h"
#include "gmu-enum-types.h"

#include <gexiv2/gexiv2.h>
#include <graphene.h>
#include <gtk/gtk.h>

/**
 * GmuExifImage:
 *
 * Contains image data and it's corresponding EXIF metadata
 *
 * `GmuExifImage` implements `GdkPaintable`.
 *
 * Since: 0.1
 */
struct _GmuExifImage
{
  GObject parent_instance;

  GExiv2Orientation orientation;
  GdkTexture *texture;
  GExiv2Metadata *metadata;
};

static void
gmu_exif_image_set_scale (GdkSnapshot *snapshot, GExiv2Orientation orientation)
{
  if (orientation == GEXIV2_ORIENTATION_HFLIP || orientation == GEXIV2_ORIENTATION_ROT_90_HFLIP)
    {
      gtk_snapshot_scale (snapshot, -1.0, 1.0);
    }
  else if (orientation == GEXIV2_ORIENTATION_VFLIP || orientation == GEXIV2_ORIENTATION_ROT_90_VFLIP)
    {
      gtk_snapshot_scale (snapshot, 1.0, -1.0);
    }
}

static void
gmu_exif_image_set_rotation (GdkSnapshot *snapshot, GExiv2Orientation orientation)
{
  switch (orientation)
    {
    case GEXIV2_ORIENTATION_ROT_90_HFLIP:
    case GEXIV2_ORIENTATION_ROT_90:
    case GEXIV2_ORIENTATION_ROT_90_VFLIP:
      gtk_snapshot_rotate (snapshot, 90);
      break;
    case GEXIV2_ORIENTATION_ROT_180:
      gtk_snapshot_rotate (snapshot, 180);
      break;
    case GEXIV2_ORIENTATION_ROT_270:
      gtk_snapshot_rotate (snapshot, 270);
      break;
    default:
      break;
    }
}

static void
gmu_exif_image_get_size (GExiv2Orientation orientation, int width, int height, int *out_width, int *out_height)
{
  if (orientation > GEXIV2_ORIENTATION_VFLIP)
    {
      *out_width = height;
      *out_height = width;
    }
  else
    {
      *out_width = width;
      *out_height = height;
    }
}

static void
gmu_exif_image_snapshot (GdkPaintable *paintable, GdkSnapshot *snapshot, double width, double height)
{
  GmuExifImage *image = GMU_EXIF_IMAGE (paintable);

  gtk_snapshot_save (snapshot);

  int tex_width, tex_height;
  gmu_exif_image_get_size (image->orientation, width, height, &tex_width, &tex_height);
  // Move to center
  gtk_snapshot_translate (snapshot, &GRAPHENE_POINT_INIT (width / 2, height / 2));
  // Handle flips
  gmu_exif_image_set_scale (snapshot, image->orientation);
  // Handle rotations
  gmu_exif_image_set_rotation (snapshot, image->orientation);
  // Move back to topleft
  gtk_snapshot_translate (snapshot, &GRAPHENE_POINT_INIT (-tex_width / 2, -tex_height / 2));
  // Draw the underlying textures
  gtk_snapshot_append_texture (snapshot, image->texture,
                               &GRAPHENE_RECT_INIT (0, 0, tex_width, tex_height));
  gtk_snapshot_restore (snapshot);
}

static GdkPaintableFlags
gmu_exif_image_get_flags (GdkPaintable *paintable)
{
  return GDK_PAINTABLE_STATIC_CONTENTS | GDK_PAINTABLE_STATIC_SIZE;
}

static int
gmu_exif_image_get_intrinsic_width (GdkPaintable *paintable)
{
  GmuExifImage *image = GMU_EXIF_IMAGE (paintable);

  switch (image->orientation)
    {
    case GEXIV2_ORIENTATION_ROT_90_HFLIP:
    case GEXIV2_ORIENTATION_ROT_90:
    case GEXIV2_ORIENTATION_ROT_90_VFLIP:
    case GEXIV2_ORIENTATION_ROT_270:
      return gdk_texture_get_height (image->texture);
    default:
      return gdk_texture_get_width (image->texture);
    }
}

static int
gmu_exif_image_get_intrinsic_height (GdkPaintable *paintable)
{
  GmuExifImage *image = GMU_EXIF_IMAGE (paintable);

  switch (image->orientation)
    {
    case GEXIV2_ORIENTATION_ROT_90_HFLIP:
    case GEXIV2_ORIENTATION_ROT_90:
    case GEXIV2_ORIENTATION_ROT_90_VFLIP:
    case GEXIV2_ORIENTATION_ROT_270:
      return gdk_texture_get_width (image->texture);
    default:
      return gdk_texture_get_height (image->texture);
    }
}

static void
gmu_exif_image_paintable_init (GdkPaintableInterface *iface)
{
  iface->snapshot = gmu_exif_image_snapshot;
  iface->get_flags = gmu_exif_image_get_flags;
  iface->get_intrinsic_width = gmu_exif_image_get_intrinsic_width;
  iface->get_intrinsic_height = gmu_exif_image_get_intrinsic_height;
}

/* When defining the GType, we need to implement the GdkPaintable interface */
G_DEFINE_TYPE_WITH_CODE (GmuExifImage,
                         gmu_exif_image,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GDK_TYPE_PAINTABLE, gmu_exif_image_paintable_init))

enum
{
  PROP_0,
  PROP_ORIENTATION,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

static void
gmu_exif_image_get_property (GObject *object,
                             guint prop_id,
                             GValue *value,
                             GParamSpec *pspec)
{
  GmuExifImage *exif_image = GMU_EXIF_IMAGE (object);

  switch (prop_id)
    {
    case PROP_ORIENTATION:
      g_value_set_enum (value, gmu_exif_image_get_orientation (exif_image));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmu_exif_image_class_init (GmuExifImageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = gmu_exif_image_get_property;

   /**
   * GmuExifImage:orientation:
   *
   * The orientation of the imagedata, specified inside EXIF metadata
   * See #GmuExifOrientation
   *
   * Since: 0.1
   */
  properties[PROP_ORIENTATION] =
      g_param_spec_enum ("orientation",
                         "Orientation",
                         "The image orientation",
                         GMU_TYPE_EXIF_ORIENTATION,
                         GMU_EXIF_ORIENTATION_UNSPECIFIED,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gmu_exif_image_init (GmuExifImage *self)
{
  self->orientation = GEXIV2_ORIENTATION_UNSPECIFIED;
}

/**
 * gmu_exif_image_get_orientation:
 * @self: a #GmuExifImage
 *
 * Gets images orientation.
 *
 * Returns: the images orientation.
 */
GmuExifOrientation
gmu_exif_image_get_orientation (GmuExifImage *self)
{
  g_return_val_if_fail (GMU_IS_EXIF_IMAGE (self), GMU_EXIF_ORIENTATION_UNSPECIFIED);

  return self->orientation;
}

static void
gmu_exif_image_set_metadata (GmuExifImage *self, GExiv2Metadata *metadata)
{
  g_return_if_fail (GMU_IS_EXIF_IMAGE (self));

  g_object_freeze_notify (G_OBJECT (self));

  if (metadata)
    g_object_ref (metadata);

  if (self->metadata)
    g_object_unref (self->metadata);

  self->metadata = metadata;

  if (metadata)
    {
      self->orientation = gexiv2_metadata_try_get_orientation (self->metadata, NULL);
    }

  g_object_thaw_notify (G_OBJECT (self));
}

static void
gmu_exif_image_set_texture (GmuExifImage *image, GdkTexture *texture)
{
  g_return_if_fail (GMU_IS_EXIF_IMAGE (image));
  g_return_if_fail (texture == NULL || GDK_IS_TEXTURE (texture));

  g_object_freeze_notify (G_OBJECT (image));

  if (texture)
    g_object_ref (texture);

  if (image->texture)
    g_object_unref (image->texture);

  image->texture = texture;

  g_object_thaw_notify (G_OBJECT (image));
}

/**
 * gmu_exif_image_new_from_file:
 * @file: a valid `GFile` handle to the file
 *
 * Creates a new `GmuExifImage` from @file.
 *
 * Returns: the new created `GmuExifImage`
 *
 * Since: 0.1
 */
GmuExifImage *
gmu_exif_image_new_from_file (GFile *file)
{
  GmuExifImage *image;

  image = g_object_new (GMU_TYPE_EXIF_IMAGE, NULL);

  gmu_exif_image_set_from_file (image, file);

  return image;
}

/**
 * gmu_exif_image_set_from_file:
 * @self: an exif image
 * @file: a valid `GFile` handle to the file
 *
 * Sets the file data for this exif image.
 *
 * Since: 0.1
 */
void
gmu_exif_image_set_from_file (GmuExifImage *self, GFile *file)
{
  g_return_if_fail (GMU_IS_EXIF_IMAGE (self));

  g_object_freeze_notify (G_OBJECT (self));

  if (file == NULL)
    {
      g_object_thaw_notify (G_OBJECT (self));
      return;
    }

  GError *error = NULL;
  GExiv2Metadata *metadata = gexiv2_metadata_new ();
  gchar *filepath = g_file_get_path (file);

  if (!gexiv2_metadata_open_path (metadata, filepath, &error))
    {
      fprintf (stderr, "Unable to read metadata from path: %s\n", error->message);
      g_error_free (error);

      g_object_thaw_notify (G_OBJECT (self));
      return;
    }

  GdkTexture *texture = gdk_texture_new_from_file (file, &error);

  if (texture == NULL)
    {
      fprintf (stderr, "Unable to read texture from path: %s\n", error->message);
      g_error_free (error);

      g_object_thaw_notify (G_OBJECT (texture));
      return;
    }

  gmu_exif_image_set_texture (self, texture);
  gmu_exif_image_set_metadata (self, metadata);

  g_object_unref (texture);
  g_object_unref (metadata);

  g_object_thaw_notify (G_OBJECT (self));
}

/**
 * gmu_exif_image_new_from_bytes:
 * @bytes: valid `GBytes` containing the data to load
 *
 * Creates a new `GmuExifImage` from @bytes.
 *
 * Returns: the new created `GmuExifImage`
 *
 * Since: 0.1
 */
GmuExifImage *
gmu_exif_image_new_from_bytes (GBytes *bytes)
{
  GmuExifImage *image;

  image = g_object_new (GMU_TYPE_EXIF_IMAGE, NULL);

  gmu_exif_image_set_from_bytes (image, bytes);

  return image;
}

/**
 * gmu_exif_image_set_from_bytes:
 * @self: an exif image
 * @bytes: a valid GFile
 *
 * Sets the file data for this exif image.
 *
 * Since: 0.1
 */
void
gmu_exif_image_set_from_bytes (GmuExifImage *self, GBytes *bytes)
{
  g_return_if_fail (GMU_IS_EXIF_IMAGE (self));

  g_object_freeze_notify (G_OBJECT (self));

  if (bytes == NULL)
    {
      g_object_thaw_notify (G_OBJECT (self));
      return;
    }

  GError *error = NULL;
  GExiv2Metadata *metadata = gexiv2_metadata_new ();

  if (!gexiv2_metadata_open_buf (metadata, g_bytes_get_data (bytes, NULL), g_bytes_get_size (bytes), &error))
    {
      fprintf (stderr, "Unable to read metadata from buffer: %s\n", error->message);
      g_error_free (error);

      g_object_thaw_notify (G_OBJECT (self));
      return;
    }

  GdkTexture *texture = gdk_texture_new_from_bytes (bytes, &error);

  if (texture == NULL)
    {
      fprintf (stderr, "Unable to read texture from buffer: %s\n", error->message);
      g_error_free (error);

      g_object_thaw_notify (G_OBJECT (texture));
      return;
    }

  gmu_exif_image_set_texture (self, texture);
  gmu_exif_image_set_metadata (self, metadata);

  g_object_unref (texture);
  g_object_unref (metadata);

  g_object_thaw_notify (G_OBJECT (self));
}

/**
 * gmu_exif_image_try_get_latitude:
 * @self: an exif image
 * @latitude: the output latitude
 *
 * Gets the latitude metadata from EXIF
 *
 * Returns: wether or not the latitude could be retrieved / was present
 *
 * Since: 0.1
 */
gboolean
gmu_exif_image_try_get_latitude (GmuExifImage *self, gdouble *latitude)
{
  return gexiv2_metadata_try_get_gps_latitude (self->metadata, latitude, NULL);
}

/**
 * gmu_exif_image_try_get_longitude:
 * @self: an exif image
 * @longitude: the output longitude
 *
 * Gets the longitude metadata from EXIF
 *
 * Returns: wether or not the longitude could be retrieved / was present
 *
 * Since: 0.1
 */
gboolean
gmu_exif_image_try_get_longitude (GmuExifImage *self, gdouble *longitude)
{
  return gexiv2_metadata_try_get_gps_longitude (self->metadata, longitude, NULL);
}
