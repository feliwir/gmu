/*
 * Copyright (c) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "gmu-ffmpeg-media.h"
#include "gmu-enum-types.h"
#include "gmu-ffmpeg-io-private.h"
#include "gmu-ffmpeg-stream-private.h"

#include <glib/gi18n.h>

#include <libavcodec/avcodec.h>
#include <libavdevice/avdevice.h>
#include <libavformat/avformat.h>

/**
 * GmuFFmpegMedia:
 *
 * Opens a media file using FFmpeg libraries. Works for audio, video & images
 *
 * `GmuFFmpegMedia` implements `GdkPaintable`.
 *
 * Since: 0.1
 */
struct _GmuFFmpegMedia
{
  GObject parent_instance;

  GError *error;

  GFile *file;
  GInputStream *input_stream;

  gboolean seeking;
  gint64 timestamp;
  gint64 duration;

  AVFormatContext *format_ctx;
  GListStore *streams;
};

enum
{
  PROP_0,
  PROP_ERROR,
  PROP_FILE,
  PROP_INPUT_STREAM,

  PROP_STREAMS,
  PROP_TIMESTAMP,
  PROP_DURATION,
  PROP_SEEKING,

  N_PROPS,
};

static GParamSpec *properties[N_PROPS] = {
  NULL,
};

static void
gmu_ffmpeg_media_paintable_init (GdkPaintableInterface *iface)
{
  //   iface->snapshot = gmu_exif_image_snapshot;
  //   iface->get_flags = gmu_exif_image_get_flags;
  //   iface->get_intrinsic_width = gmu_exif_image_get_intrinsic_width;
  //   iface->get_intrinsic_height = gmu_exif_image_get_intrinsic_height;
}

/* When defining the GType, we need to implement the GdkPaintable interface */
G_DEFINE_TYPE_WITH_CODE (GmuFFmpegMedia,
                         gmu_ffmpeg_media,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GDK_TYPE_PAINTABLE, gmu_ffmpeg_media_paintable_init))

static void
gmu_ffmpeg_media_open (GmuFFmpegMedia *self);
static void
gmu_ffmpeg_media_close (GmuFFmpegMedia *self);

static void
gmu_ffmpeg_media_gerror (GmuFFmpegMedia *self,
                         GError *error)
{
  g_return_if_fail (GMU_IS_FFMPEG_MEDIA (self));
  g_return_if_fail (error != NULL);

  if (self->error)
    {
      g_error_free (error);
      return;
    }

  g_object_freeze_notify (G_OBJECT (self));

  self->error = error;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ERROR]);

  g_object_thaw_notify (G_OBJECT (self));
}

static void
gmu_ffmpeg_media_error (GmuFFmpegMedia *self,
                        GQuark domain,
                        int code,
                        const char *format,
                        ...)
{
  GError *error;
  va_list args;

  g_return_if_fail (GMU_IS_FFMPEG_MEDIA (self));
  g_return_if_fail (domain != 0);
  g_return_if_fail (format != NULL);

  va_start (args, format);
  error = g_error_new_valist (domain, code, format, args);
  va_end (args);

  gmu_ffmpeg_media_gerror (self, error);
}

static void
gmu_ffmpeg_media_set_ffmpeg_error (GmuFFmpegMedia *self,
                                   int av_errnum)
{
  char s[AV_ERROR_MAX_STRING_SIZE];

  if (self->error)
    return;

  if (av_strerror (av_errnum, s, sizeof (s) != 0))
    g_snprintf (s, sizeof (s), _("Unspecified media error"));

  gmu_ffmpeg_media_error (self,
                          G_IO_ERROR,
                          G_IO_ERROR_FAILED,
                          "%s",
                          s);
}

/**
 * gmu_ffmpeg_media_new_for_file:
 * @file: The file to play
 *
 * Creates a new ffmpeg media to play @file.
 *
 * Returns: (type Gmu.FFmpegMedia): a new `GmuFFmpegMedia` playing @file
 */
GmuFFmpegMedia *
gmu_ffmpeg_media_new_for_file (GFile *file)
{
  g_return_val_if_fail (file == NULL || G_IS_FILE (file), NULL);

  return g_object_new (GMU_TYPE_FFMPEG_MEDIA,
                       "file", file,
                       NULL);
}

/**
 * gmu_ffmpeg_media_new_for_input_stream:
 * @stream: The stream to play
 *
 * Creates a new media file to play @stream.
 *
 * If you want the resulting media to be seekable,
 * the stream should implement the `GSeekable` interface.
 *
 * Returns: (type Gmu.FFmpegMedia): a new `GmuFFmpegMedia`
 */
GmuFFmpegMedia *
gmu_ffmpeg_media_new_for_input_stream (GInputStream *stream)
{
  g_return_val_if_fail (stream == NULL || G_IS_INPUT_STREAM (stream), NULL);

  return g_object_new (GMU_TYPE_FFMPEG_MEDIA,
                       "input-stream", stream,
                       NULL);
}

/**
 * gmu_ffmpeg_media_set_file: (attributes org.gtk.Method.set_property=file)
 * @self: a `GmuFFmpegMedia`
 * @file: (nullable): the file to play
 *
 * Sets the `GmuFFmpegMedia` to play the given file.
 *
 * If any file is still playing, stop playing it.
 */
void
gmu_ffmpeg_media_set_file (GmuFFmpegMedia *self,
                           GFile *file)
{
  g_return_if_fail (GMU_IS_FFMPEG_MEDIA (self));
  g_return_if_fail (file == NULL || G_IS_FILE (file));

  if (file)
    g_object_ref (file);

  g_object_freeze_notify (G_OBJECT (self));

  gmu_ffmpeg_media_clear (self);

  if (file)
    {
      self->file = file;
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_FILE]);
      gmu_ffmpeg_media_open (self);
    }

  g_object_thaw_notify (G_OBJECT (self));
}

/**
 * gmu_ffmpeg_media_is_seeking:
 * @self: a `GmuFFmpegMedia`
 *
 * Checks if there is currently a seek operation going on.
 *
 * Returns: %TRUE if a seek operation is ongoing.
 */
gboolean
gmu_ffmpeg_media_is_seeking (GmuFFmpegMedia *self)
{
  g_return_val_if_fail (GMU_IS_FFMPEG_MEDIA (self), FALSE);

  return self->seeking;
}

/**
 * gmu_ffmpeg_media_get_file: (attributes org.gtk.Method.get_property=file)
 * @self: a `GmuFFmpegMedia`
 *
 * Returns the file that @self is currently playing from.
 *
 * When @self is not playing or not playing from a file,
 * %NULL is returned.
 *
 * Returns: (nullable) (transfer none): The currently playing file
 */
GFile *
gmu_ffmpeg_media_get_file (GmuFFmpegMedia *self)
{
  g_return_val_if_fail (GMU_IS_FFMPEG_MEDIA (self), NULL);

  return self->file;
}

/**
 * gmu_ffmpeg_media_set_input_stream: (attributes org.gtk.Method.set_property=input-stream)
 * @self: a `GmuFFmpegMedia`
 * @stream: (nullable): the stream to play from
 *
 * Sets the `GmuFFmpegMedia` to play the given stream.
 *
 * If anything is still playing, stop playing it.
 *
 * Full control about the @stream is assumed for the duration of
 * playback. The stream will not be closed.
 */
void
gmu_ffmpeg_media_set_input_stream (GmuFFmpegMedia *self,
                                   GInputStream *stream)
{
  g_return_if_fail (GMU_IS_FFMPEG_MEDIA (self));
  g_return_if_fail (stream == NULL || G_IS_INPUT_STREAM (stream));

  if (stream)
    g_object_ref (stream);

  g_object_freeze_notify (G_OBJECT (self));

  gmu_ffmpeg_media_clear (self);

  if (stream)
    {
      self->input_stream = stream;
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_INPUT_STREAM]);
      gmu_ffmpeg_media_open (self);
    }

  g_object_thaw_notify (G_OBJECT (self));
}

/**
 * gmu_ffmpeg_media_get_input_stream: (attributes org.gtk.Method.get_property=input-stream)
 * @self: a `GmuFFmpegMedia`
 *
 * Returns the stream that @self is currently playing from.
 *
 * When @self is not playing or not playing from a stream,
 * %NULL is returned.
 *
 * Returns: (nullable) (transfer none): The currently playing stream
 */
GInputStream *
gmu_ffmpeg_media_get_input_stream (GmuFFmpegMedia *self)
{
  g_return_val_if_fail (GMU_IS_FFMPEG_MEDIA (self), NULL);

  return self->input_stream;
}

/**
 * gmu_ffmpeg_media_get_timestamp:
 * @self: a `GmuFFmpegMedia`
 *
 * Returns the current timestamp of the media
 *
 * Returns: the current timestamp in microseconds
 */
gint64
gmu_ffmpeg_media_get_timestamp (GmuFFmpegMedia *self)
{
  g_return_val_if_fail (GMU_IS_FFMPEG_MEDIA (self), 0);

  return self->timestamp;
}

/**
 * gmu_ffmpeg_media_get_duration: 
 * @self: a `GmuFFmpegMedia`
 *
 * Returns the overall duration of the media
 * 
 * Returns: the duration of the media in microseconds or 0 if unknown
 */
gint64
gmu_ffmpeg_media_get_duration (GmuFFmpegMedia *self)
{
  g_return_val_if_fail (GMU_IS_FFMPEG_MEDIA (self), 0);

  return self->duration;
}

static void
gmu_ffmpeg_media_set_property (GObject *object,
                               guint prop_id,
                               const GValue *value,
                               GParamSpec *pspec)

{
  GmuFFmpegMedia *self = GMU_FFMPEG_MEDIA (object);

  switch (prop_id)
    {
    case PROP_FILE:
      gmu_ffmpeg_media_set_file (self, g_value_get_object (value));
      break;

    case PROP_INPUT_STREAM:
      gmu_ffmpeg_media_set_input_stream (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gmu_ffmpeg_media_get_property (GObject *object,
                               guint prop_id,
                               GValue *value,
                               GParamSpec *pspec)
{
  GmuFFmpegMedia *self = GMU_FFMPEG_MEDIA (object);

  switch (prop_id)
    {
    case PROP_ERROR:
      g_value_set_boxed (value, self->error);
      break;

    case PROP_FILE:
      g_value_set_object (value, self->file);
      break;

    case PROP_INPUT_STREAM:
      g_value_set_object (value, self->input_stream);
      break;

    case PROP_SEEKING:
      g_value_set_boolean (value, self->seeking);
      break;

     case PROP_TIMESTAMP:
      g_value_set_int64 (value, self->timestamp);
      break;

    case PROP_DURATION:
      g_value_set_int64 (value, self->duration);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gmu_ffmpeg_media_dispose (GObject *object)
{
  GmuFFmpegMedia *self = GMU_FFMPEG_MEDIA (object);

  g_clear_object (&self->file);
  g_clear_object (&self->input_stream);

  G_OBJECT_CLASS (gmu_ffmpeg_media_parent_class)->dispose (object);
}

static gboolean
gmu_ffmpeg_media_is_open (GmuFFmpegMedia *self)
{
  return self->file || self->input_stream;
}

/**
 * gmu_ffmpeg_media_clear:
 * @self: a `GmuFFmpegMedia`
 *
 * Resets the media file to be empty.
 */
void
gmu_ffmpeg_media_clear (GmuFFmpegMedia *self)
{
  g_return_if_fail (GMU_IS_FFMPEG_MEDIA (self));

  if (!gmu_ffmpeg_media_is_open (self))
    return;

  gmu_ffmpeg_media_close (self);

  // TODO: this leaks here. Need to free all elements in list
  if (self->streams)
    {
      g_list_store_remove_all (self->streams);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_STREAMS]);
    }
  if (self->input_stream)
    {
      g_clear_object (&self->input_stream);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_INPUT_STREAM]);
    }
  if (self->file)
    {
      g_clear_object (&self->file);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_FILE]);
    }
}

static void
gmu_ffmpeg_media_class_init (GmuFFmpegMediaClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->set_property = gmu_ffmpeg_media_set_property;
  gobject_class->get_property = gmu_ffmpeg_media_get_property;
  gobject_class->dispose = gmu_ffmpeg_media_dispose;

  /**
   * GmuFFmpegMedia:error:
   *
   * %NULL for a properly working stream or the `GError`
   * that the stream is in.
   */
  properties[PROP_ERROR] =
      g_param_spec_boxed ("error", NULL, NULL,
                          G_TYPE_ERROR,
                          G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * GmuFFmpegMedia:file: (attributes org.gtk.Property.get=gmu_ffmpeg_media_get_file)
   *
   * The file being played back or %NULL if not playing a file.
   */
  properties[PROP_FILE] =
      g_param_spec_object ("file", NULL, NULL,
                           G_TYPE_FILE,
                           G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * GmuFFmpegMedia:input-stream: (attributes org.gtk.Property.get=gmu_ffmpeg_media_get_input_stream)
   *
   * The stream being played back or %NULL if not playing a stream.
   *
   * This is %NULL when playing a file.
   */
  properties[PROP_INPUT_STREAM] =
      g_param_spec_object ("input-stream", NULL, NULL,
                           G_TYPE_INPUT_STREAM,
                           G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);


  /**
   * GmuFFmpegMedia:timestamp:
   *
   * The current presentation timestamp in microseconds.
   */
  properties[PROP_TIMESTAMP] =
    g_param_spec_int64 ("timestamp", NULL, NULL,
                        0, G_MAXINT64, 0,
                        G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * GmuFFmpegMedia:duration:
   *
   * The media's duration in microseconds or 0 if unknown.
   */
  properties[PROP_DURATION] =
    g_param_spec_int64 ("duration", NULL, NULL,
                        0, G_MAXINT64, 0,
                        G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * GmuFFmpegMedia:streams:
   *
   * The media's streams.
   */
  properties[PROP_STREAMS] =
    g_param_spec_object ("streams", NULL, NULL,
                        G_TYPE_LIST_MODEL,
                        G_PARAM_READABLE);

   /**
   * GmuFFmpegMedia:seeking:
   *
   * Set while a seek is in progress.
   */
  properties[PROP_SEEKING] =
    g_param_spec_boolean ("seeking", NULL, NULL,
                          FALSE,
                          G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (gobject_class, N_PROPS, properties);
}

static void
gmu_ffmpeg_media_init (GmuFFmpegMedia *self)
{
  self->streams = g_list_store_new(GMU_TYPE_FFMPEG_STREAM);
}

static gboolean
gmu_ffmpeg_media_create_input_stream (GmuFFmpegMedia *self)
{
  GError *error = NULL;

  if (self->file)
    {
      self->input_stream = G_INPUT_STREAM (g_file_read (self->file, NULL, &error));
      if (self->input_stream == NULL)
        {
          gmu_ffmpeg_media_gerror (self, error);
          g_error_free (error);
          return FALSE;
        }
    }
  else if (!self->input_stream)
    {
      return FALSE;
    }

  return TRUE;
}

static void
gmu_ffmpeg_media_open (GmuFFmpegMedia *self)
{
  int errnum;

  if (!gmu_ffmpeg_media_create_input_stream (self))
    {
      gmu_ffmpeg_media_error (self,
                              G_IO_ERROR,
                              G_IO_ERROR_FAILED,
                              _("Cannot create input stream"));
      return;
    }

  self->format_ctx = avformat_alloc_context ();
  self->format_ctx->pb = gmu_ff_create_input_io_context (self->input_stream);
  if (self->format_ctx->pb == NULL)
    {
      gmu_ffmpeg_media_error (self,
                              G_IO_ERROR,
                              G_IO_ERROR_FAILED,
                              _("Not enough memory"));
      return;
    }
  errnum = avformat_open_input (&self->format_ctx, NULL, NULL, NULL);
  if (errnum != 0)
    {
      gmu_ffmpeg_media_set_ffmpeg_error (self, errnum);
      return;
    }

  errnum = avformat_find_stream_info (self->format_ctx, NULL);
  if (errnum < 0)
    {
      gmu_ffmpeg_media_set_ffmpeg_error (self, errnum);
      return;
    }

  // Create streams
  for (int stream_id = 0; stream_id < self->format_ctx->nb_streams; stream_id++)
    {
      AVStream *stream = self->format_ctx->streams[stream_id];
      g_list_store_append (self->streams, gmu_ffmpeg_stream_new_from_avdata (stream));
    }

  // Set duration
  if (self->format_ctx->duration != AV_NOPTS_VALUE)
    {
      self->duration = av_rescale (self->format_ctx->duration, G_USEC_PER_SEC, AV_TIME_BASE);
    }

  gmu_ffmpeg_media_process(self);
}

static void
gmu_ffmpeg_media_close (GmuFFmpegMedia *self)
{
  avformat_close_input (&self->format_ctx);
}

/**
 * gmu_ffmpeg_media_seek:
 * @self: a `GmuFFmpegMedia`
 * @timestamp: timestamp to seek to.
 *
 * Start a seek operation on @self to @timestamp.
 *
 * If @timestamp is out of range, it will be clamped.
 *
 * Seek operations may not finish instantly. While a
 * seek operation is in process, the seeking
 * property will be set.
 *
 * When calling gmu_ffmpeg_media_seek() during an
 * ongoing seek operation, the new seek will override
 * any pending seek.
 */
gboolean
gmu_ffmpeg_media_seek (GmuFFmpegMedia *self, int64_t timestamp)
{
  int errnum;
  int stream_id = 0;
  GmuFFmpegStream *stream;
  AVStream *avstream;
  gboolean was_seeking;
  g_return_val_if_fail (GMU_IS_FFMPEG_MEDIA (self), FALSE);

  g_object_freeze_notify (G_OBJECT (self));

  was_seeking = self->seeking;
  self->seeking = TRUE;

  for (stream_id = 0; stream_id < g_list_model_get_n_items (G_LIST_MODEL(self->streams)); stream_id++)
    {
      stream = GMU_FFMPEG_STREAM (g_list_model_get_item (G_LIST_MODEL (self->streams), stream_id));
      avstream = gmu_ffmpeg_stream_get_stream (stream);

      errnum = av_seek_frame (self->format_ctx,
                              stream_id,
                              av_rescale_q (timestamp,
                                            (AVRational){ 1, G_USEC_PER_SEC },
                                            avstream->time_base),
                              AVSEEK_FLAG_BACKWARD);

      if (errnum < 0)
        {
          gmu_ffmpeg_media_set_ffmpeg_error (self, errnum);
          return FALSE;
        }

      stream_id++;
    }

  if (was_seeking != self->seeking)
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SEEKING]);

  g_object_thaw_notify (G_OBJECT (self));

  return TRUE;
}

gboolean
gmu_ffmpeg_media_process (GmuFFmpegMedia *self)
{
  AVPacket packet;
  GmuFFmpegStream *stream;
  int errnum;

  for (errnum = av_read_frame (self->format_ctx, &packet);
       errnum >= 0;
       errnum = av_read_frame (self->format_ctx, &packet))
    {
      stream = GMU_FFMPEG_STREAM (g_list_model_get_item(G_LIST_MODEL(self->streams), packet.stream_index));
      if(gmu_ffmpeg_stream_process_packet(stream, &packet))
      {
        break;
      }
    }

  return TRUE;
}